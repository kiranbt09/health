"use strict"

const jwt = require('jsonwebtoken');
const SECRET_KEY = "Grocery__JWT_Secret";

module.exports = {
  randomNum: () => {
    return Math.floor(100000 + Math.random() * 900000)
  },
  generateJWTToken: (payload) => {
    return jwt.sign(payload, SECRET_KEY);
  },
  verifyJWTToken: (token) => {
    return jwt.verify(token, SECRET_KEY);
  },
};