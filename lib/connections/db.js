"use strict"

const mysql = require('mysql');

module.exports = mysql.createConnection({
  host: process.env.DB_HOST || '0.0.0.0',
  user: process.env.DB_USERNAME || 'health_guard',
  password: process.env.DB_PASSWORD || 'healthGuard',
  database: process.env.DB_NAME || 'health_guard',
});
