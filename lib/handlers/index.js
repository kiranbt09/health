"use strict"

const Boom = require('@hapi/boom');
const userModel = require('../models/user_model');
const productModel = require('../models/product_model');
const orderModel = require('../models/order_model');

//Defining external methods
const internals = module.exports = (route, options) => {
  return (request, h) => {
    return internals[options.method](request, h);
  };
};

internals.getProducts = async(request, h) => {
  try {
    return await productModel.getProducts(request);
  } catch (err) {
    return Boom.unauthorized('Failed to get product details');
  }
};

internals.getCheckout = async(request, h) => {
  try {
    return await productModel.getCheckout(request);
  } catch (err) {
    return Boom.unauthorized('Failed to get checkout details');
  }
};

internals.createOrder = async(request, h) => {
  try {
    return await orderModel.createOrder(request);
  } catch (err) {
    return Boom.unauthorized('Failed to create order');
  }
};

internals.getOrders = async(request, h) => {
  try {
    return await orderModel.getOrders(request);
  } catch (err) {
    return Boom.notFound('Failed to get orders list');
  }
};

internals.getOrder = async(request, h) => {
  try {
    return await orderModel.getOrder(request);
  } catch (err) {
    return Boom.notFound('Failed to get order details');
  }
};


internals.register = async(request, h) => {
  try {
    const users = await userModel.validateUser(request.payload.mobile);
    const userDetails = JSON.parse(JSON.stringify(users));
    if (userDetails[0].count == 0) {
      return await userModel.createUser(request);
    }else{
      return Boom.conflict('User already exist.');
    }
  } catch (err) {
    return Boom.badImplementation(err);
  }
};

internals.validateLogin = async(request, h) => {
  try {
    const users = await userModel.validateUser(request.payload.mobile);
    const userDetails = JSON.parse(JSON.stringify(users));
    if (userDetails[0].count > 0) {
      return await userModel.updateUser(userDetails[0], request);
    }
    return await userModel.createUser(request);
  } catch (err) {
    return Boom.badImplementation(err);
  }
};

internals.validateUser = async(request, h) => {
  try {
    const user = await userModel.verifyOtp(request.payload.mobile, request.payload.otp);
    const userDetails = JSON.parse(JSON.stringify(user));
    if (userDetails[0].count > 0) {
      return await userModel.updateVerificationStatus(userDetails[0]);
    } else 
      return Boom.notFound('Invalid OTP');
  } catch (err) {
    return Boom.badImplementation(err);
  }
};

internals.getUserDetails = async(request, h) => {
  try {
    return await userModel.getUserDetails(request);
  } catch (err) {
    return Boom.unauthorized('Invalid access token');
  }
};

internals.updateUserDetails = async(request, h) => {
  try {
    return await userModel.updateUserDetails(request);
  } catch (err) {
    return Boom.unauthorized('Invalid access token');
  }
};