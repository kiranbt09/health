"use strict";

const Joi = require('joi');

// Importing `index` handler from `handler/index.js` file
const handler = require('../handlers/index');

module.exports = function() {
	return [
		{
			method: 'POST',
			path: '/api/register',
			config: {
				handler: handler.register,
				description: 'To register the user',
				notes: 'Returns user success response',
				tags: ['api'],
				validate: {
					payload: {
						name: Joi.string()
										.example('Nagesh'),
						mobile: Joi.number()
										.integer()
										.min(1000000000)
										.max(9999999999)
										.required()
										.description('User phone number')
										.example('9898989898'),
						email: Joi.string()
										.email()
										.example('user@example.com'),
						password: Joi.string()
										.example('P*@ssw0rd'),
					},
				},
			},
		},
		{
			method: 'POST',
			path: '/api/login',
			config: {
				handler: handler.validateLogin,
				description: 'To login the user',
				notes: 'Returns user success response',
				tags: ['api'],
				validate: {
					payload: {
						email: Joi.string()
										.email()
										.example('user@example.com'),
						mobile: Joi.number()
										.integer()
										.min(1000000000)
										.max(9999999999)
										.required()
										.description('User phone number')
										.example('9898989898'),
						source: Joi.string()
										.required()
										.valid('otp', 'truecaller')
										.description('Source of login')
										.example('otp'),
						lat: Joi.string()
										.invalid('20.9517', '27.5330', '23.6102', '18.1124')
										.required()
										.description('User latitude')
										.example('17.1284'),
						lang: Joi.string()
										.invalid('85.0985', '88.5122', '85.2799', '79.0193')
										.required()
										.description('User latitude')
										.example('78.1234'),
					},
				},
			},
		},
		{
			method: 'POST',
			path: '/api/validate',
			config: {
				handler: handler.validateUser,
				description: 'To validate the user',
				notes: 'Returns user success response with token',
				tags: ['api'],
				validate: {
					payload: {
						mobile : Joi.number()
										.integer()
										.min(1000000000)
										.max(9999999999)
										.required()
										.description('User phone number')
										.example('9898989898'),
						otp : Joi.number()
										.min(100000)
										.max(900000)
										.required()
										.description('User otp number')
										.example('123456'),
					},
				},
			},
		},
		{
			method: 'GET',
			path: '/api/profile',
			config: {
				handler: handler.getUserDetails,
				description: 'To get the user profile information',
				notes: 'Returns user profile data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						token: Joi.string().required(),
						accept: Joi.string().valid('application/json').required(),
					}).unknown(),
				},
			},
		},
		{
			method: 'PUT',
			path: '/api/profile',
			config: {
				handler: handler.updateUserDetails,
				description: 'To update the user profile information',
				notes: 'Updates user profile data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						token: Joi.string().required(),
						accept: Joi.string().valid('application/json').required(),
					}).unknown(),
					payload: Joi.object().keys({
						email: Joi.string()
										.email()
										.description('User email')
										.example('user@example.com'),
						firstName: Joi.string()
										.description('Enter fist name')
										.example('Royal'),
						lastName: Joi.string()
										.description('Enter last name')
										.example('Grocery'),
						gender: Joi.string()
										.valid('male', 'female')
										.description('Enter gender')
										.example('male'),
						dob: Joi.string()
										.regex(/^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/)
										.description('Enter date of birth')
										.example('29-01-1996'),
						profilePic: Joi.string()
													.description('Enter profile picture url')
													.example('http://example.com/profile.jpg'),
					}).min(1),
				},
			},
		},
		{
			method: 'GET',
			path: '/api/products',
			config: {
				handler: handler.getProducts,
				description: 'To get the product list',
				notes: 'Returns products data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						category: Joi.string().required(),
					}).unknown(),
				},
			},
		},
		{
			method: 'POST',
			path: '/api/getCheckout',
			config: {
				handler: handler.getCheckout,
				description: 'To get the checkout details',
				notes: 'Returns checkout data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						accept: Joi.string().valid('application/json').required(),
					}).unknown(),
					payload: Joi.object().keys({
						mobile: Joi.number()
										.integer()
										.min(1000000000)
										.max(9999999999)
										.required()
										.description('User phone number')
										.example('9898989898'),
						product_ids: Joi.string()
										.description('product ids with comma separated string')
										.example('102,109,XXX'),
					})
				},
			},
		},
		{
			method: 'POST',
			path: '/api/createOrder',
			config: {
				handler: handler.createOrder,
				description: 'To create the order',
				notes: 'Returns order data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						accept: Joi.string().valid('application/json').required(),
					}).unknown(),
					payload: Joi.object().keys({
						user_id: Joi.number()
										.integer()
										.example('235'),
						product_ids: Joi.string()
										.description('product ids with comma separated string')
										.example('102,109,XXX'),
					})
				},
			},
		},
		{
			method: 'GET',
			path: '/api/orders',
			config: {
				handler: handler.getOrders,
				description: 'To get the orders list',
				notes: 'Returns orders data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						user_id: Joi.string().required(),
					}).unknown(),
				},
			},
		},
		{
			method: 'GET',
			path: '/api/order',
			config: {
				handler: handler.getOrder,
				description: 'To get the order data',
				notes: 'Returns order data',
				tags: ['api'],
				validate: {
					headers: Joi.object().keys({
						order_id: Joi.string().required(),
					}).unknown(),
				},
			},
		},
	];
}();
