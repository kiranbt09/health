"use strict";

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package');
const routes = require('./routes/route');
const db = require('./connections/db');
const serverConfig = require('./connections/config');
const { host, port } = serverConfig.config;

(async () => {

  db.connect(function(err) {
    if (err) throw new Error(err);
  });

  const server = await new Hapi.Server({
    host,
    port,
  });

  const swaggerOptions = {
    info: {
      title: Pack.name,
      version: Pack.version,
    },
  };

  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);
  try {
    await server.start();
    console.log('Server running at:', server.info.uri);
  } catch(err) {
    throw new Error(err);
  }

  // Add all the routes within the routes folder
  for (var route in routes) {
    //Define routes
    server.route(routes[route]);
  }
})();