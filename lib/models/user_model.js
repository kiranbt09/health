"use strict"

const db = require('../connections/db');
const { uuid } = require('uuidv4');
const helpers = require('../helpers/helper');
const { generateJWTToken } = require('../helpers/helper');

module.exports = {
  createUser: (request) => {
    return new Promise((resolve, reject) => {
      const id = uuid();
      const sql = `INSERT INTO user (id, first_name, phone, email_id, password, verification_status, validation_status)
        VALUES
        ('${id}', '${request.payload.name}', '${request.payload.mobile}', '${request.payload.email}', '${request.payload.password}', 'verified', 'valid')
      `;
      db.query(sql, (err, res) => {
        if (err) reject(err);
        let token = '';
        token = generateJWTToken({id,});
        resolve({
          status: true,
          code: 200,
          message: 'User registed successfully',
          verified: 'verified',
          user: {
            id,
            mobile: request.payload.mobile,
            email: request.payload.email,
            token,
          },
        });
      });
    });
  },
  validateUser: (phone = null) => {
    return new Promise((resolve, reject) => {
      const sql = `SELECT COUNT(*) AS count, email_id, phone, id, verification_status FROM user WHERE phone='${phone}'`;
      db.query(sql, async (err, res) => {
        if(err) reject(err);
        resolve(res);
      });
    });
  },
  updateUser: (userDetails, request) => {
    return new Promise((resolve, reject) => {
      const otp =  request.payload.source === 'truecaller' ? '' : helpers.randomNum();
      const verified = request.payload.source === 'truecaller' ? 1 : 0;
      const validationStatus = request.payload.source === 'truecaller' ? 'valid' : 'invalid';
      const sql = `UPDATE user SET email_id='${request.payload.email ? request.payload.email : null}', verification_status='${verified}', validation_status='${validationStatus}', source='${request.payload.source}', otp='${otp}' WHERE id='${userDetails.id}'`;
      db.query(sql, async (err, res) => {
        if(err) reject(err);
        let token = '';
        if (request.payload.source === 'truecaller') {
          token = generateJWTToken({
            id: userDetails.id,
          });
        }
        resolve({
          status: true,
          code: 200,
          message: 'User is already registered',
          verified: Boolean(Number(userDetails.verification_status)),
          user: {
            id: userDetails.id,
            mobile: userDetails.phone,
            email: request.payload.email || userDetails.email_id,
            otp,
            token,
          },
        });
      });
    });
  },
  verifyOtp: (phone = null, otp = null) => {
    return new Promise((resolve, reject) => {
      const sql = `SELECT COUNT(*) AS count, id, email_id, phone FROM user WHERE phone='${phone}' AND otp='${otp}'`;
      db.query(sql, async (err, res) => {
        if(err) reject(err);
        resolve(res);
      });
    });
  },
  updateVerificationStatus: (userDetails) => {
    return new Promise((resolve, reject) => {
      const otp = helpers.randomNum();
      const sql = `UPDATE user SET validation_status='valid', verification_status=1, otp='' WHERE id='${userDetails.id}'`;
      db.query(sql, async (err, res) => {
        if(err) reject(err);
        const token = helpers.generateJWTToken({
          id: userDetails.id,
        });
        resolve({
          status: true,
          code: 200,
          message: 'User verified successfully!',
          verified: true,
          user: {
            id: userDetails.id,
            mobile: userDetails.phone,
            email: userDetails.email_id,
            token,
          },
        });
      });
    });
  },
  getUserDetails: (request) => {
    return new Promise((resolve, reject) => {
      const userToken = helpers.verifyJWTToken(request.headers.token);
      const sql = `SELECT * FROM user WHERE id='${userToken.id}'`;
      db.query(sql, async(err, res) => {
        if(err) reject(err);
        const userData = JSON.parse(JSON.stringify(res))[0];
        let name = userData.first_name || null;
        if (userData.last_name) {
          name += ` ${userData.last_name}`;
        }
        resolve({
          status: true,
          code: 200,
          message: 'User profile details',
          user: {
            id: userData.id,
            email: userData.email_id || null,
            mobile: userData.phone,
            name,
            dob: userData.dob || null,
            gender: userData.gender || null,
            lat: userData.lat,
            lang: userData.lang,
            profilePic: userData.profile_photo || null,
            referalCode: userData.referal_code || null,
            referredBy: userData.referred_by || null,
            created: userData.created,
            updated: userData.updated,
          },
        });
      });
    });
  },
  updateUserDetails: (request) => {
    return new Promise((resolve, reject) => {
      const userToken = helpers.verifyJWTToken(request.headers.token);
      const payload = request.payload;
      let fields = payload.email ? `email_id='${payload.email}'` : '';
      fields += payload.firstName ? `, first_name='${payload.firstName}'` : '';
      fields += payload.lastName ? `, last_name='${payload.lastName}'` : '';
      fields += payload.gender ? `, gender='${payload.gender}'` : '';
      fields += payload.dob ? `, dob='${payload.dob}'` : '';
      fields += payload.profilePic ? `, profile_photo='${payload.profilePic}'` : '';

      const sql = `UPDATE user SET ${fields} WHERE id='${userToken.id}'`;
      db.query(sql, async (err, res) => {
        if(err) reject(err);
        resolve({
          status: true,
          code: 200,
          message: 'User profile details updated successfully!',
          verified: true,
          user: payload,
        });
      });
    });
  },
};