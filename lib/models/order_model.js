"use strict"

const db = require('../connections/db');
const { uuid } = require('uuidv4');
const helpers = require('../helpers/helper');
const { generateJWTToken } = require('../helpers/helper');

module.exports = {
  createOrder: (request) => {
    return new Promise((resolve, reject) => {   
      const product_ids = request.payload.product_ids.split(',');
      let product_ids_string = '';
      for (const id in product_ids) {
        product_ids_string = product_ids_string+`'${product_ids[id]}',`;
      }
      product_ids_string =  product_ids_string.substring(0, product_ids_string.length - 1);
      const sql = `SELECT * FROM products WHERE product_id IN (${product_ids_string})`;
      db.query(sql, async(err, res) => {
        if(err) reject(err);
        const productsData = JSON.parse(JSON.stringify(res));
        let totalPrice = 0;

        for (const product in productsData) {
          totalPrice = totalPrice + parseFloat(productsData[product].product_price);
        }

      const sql = `INSERT INTO orders (user_id, product_ids, total_amount, order_status, payment_mode, delivery_status)
        VALUES
        ('${request.payload.user_id}', '${request.payload.product_ids}', '${totalPrice}', 'success', 'COD', 'initiated')
      `;
      db.query(sql, (err, res1) => {
        if (err) reject(err);

      let user = {};
      const sql1 = `SELECT * FROM user WHERE id='${request.payload.user_id}'`;
      db.query(sql1, async(err, res) => {
        if(err) reject(err);
        const userData = JSON.parse(JSON.stringify(res))[0];
        let name = userData.first_name || null;
        if (userData.last_name) {
          name += ` ${userData.last_name}`;
        }
      user =  {
            id: userData.id,
            email: userData.email_id || null,
            mobile: userData.phone,
            name,
            dob: userData.dob || null,
            gender: userData.gender || null,
            lat: userData.lat,
            lang: userData.lang,
            profilePic: userData.profile_photo || null,
            referalCode: userData.referal_code || null,
            referredBy: userData.referred_by || null,
            created: userData.created,
            updated: userData.updated,
          }

          const sql1 = `SELECT * FROM orders WHERE order_id='${res1.insertId}'`;
          db.query(sql1, async(err, res) => {
            if(err) reject(err);
            const orderData = JSON.parse(JSON.stringify(res))[0];
                
              resolve({
                status: true,
                code: 200,
                message: 'Order created successfully',
                products_count: productsData.length,
                totalPrice,
                products: productsData,
                order: orderData,
                user
              });
          });
      });

      });
      
      });
    });
  },
  getOrders: (request) => {
    return new Promise((resolve, reject) => {   
      const sql1 = `SELECT * FROM orders WHERE user_id='${request.headers.user_id}'`;
      db.query(sql1, async(err, res) => {
        if(err) reject(err);
        const orderData = JSON.parse(JSON.stringify(res));
          resolve({
            status: true,
            code: 200,
            message: 'Orders data',
            orders_count: orderData.length,
            orders: orderData,
          });
      });
    });
  },
  getOrder: (request) => {
    return new Promise((resolve, reject) => {   
      const sql1 = `SELECT * FROM orders WHERE order_id='${request.headers.order_id}'`;
      db.query(sql1, async(err, res) => {
        if(err) reject(err);
        const orderData = JSON.parse(JSON.stringify(res))[0];
        if(!orderData){
          reject({
            status: true,
            code: 200,
            message: 'Order not found',
            order: {},
          });
        }
          resolve({
            status: true,
            code: 200,
            message: 'Order data',
            order: orderData,
          });
      });
    });
  },
};